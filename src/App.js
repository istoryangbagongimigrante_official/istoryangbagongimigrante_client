import './App.css';
import { Container,Row } from 'react-bootstrap'

import { useState,useEffect } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import Routes from './Routes.jsx'
import Footer from './components/Footer/Footer.jsx'
import Nav from './components/Nav/Nav.jsx'
import Register from './components/Auth/Register.jsx'
import Login from './components/Auth/Login.jsx'
import {UserProvider} from './utils/userContext.js'

function App() {
  const [isScrolling,setIsScrolling] = useState(false)
  const [selected,setSelected] = useState('home')
  const [showMobileNav,setShowMobileNav] = useState(false)
  const [startIndex,setStartIndex] = useState(1)
  const [endIndex,setEndIndex] = useState(5)
  const [end,setEnd] = useState(false)
  const [withAccount,setWithAccount] = useState(false)
  const [showAuth,setShowAuth] = useState(false)
  const [user,setUser] = useState({
    email: localStorage.getItem('email'),
    isAdmin: false
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(()=>{
    fetch('https://istoryangbagongimigrante.herokuapp.com/api/users/getUser',{
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      setUser({
        email:data.email,
        isAdmin: data.isAdmin
      })
    })    
  },[])

  return (
    <UserProvider value={{user,setUser, unsetUser}}>
      <Container fluid className="App">
        <div className="hamburger">
          {
            showMobileNav
            ?
            <i className="fas fa-chevron-circle-up" onClick={()=>setShowMobileNav(false)}></i>
           :
           <i className="fas fa-chevron-circle-down" onClick={()=>setShowMobileNav(true)}></i>
          }
        </div>
        <Row className={`auth-wrapper ${showAuth?'show-auth':null}`}>
          {
            withAccount
            ?
            <Login
              setWithAccount={setWithAccount}
              setShowAuth={setShowAuth}
            />
            :
            <Register
              setWithAccount={setWithAccount}
              setShowAuth={setShowAuth}
            />
          }
        </Row>
      
        <Router>
          <Row className="nav-wrapper">
            <Nav
              selected={selected}
              setSelected={setSelected}
              showMobileNav={showMobileNav}
              setShowMobileNav={setShowMobileNav}
              isScrolling={isScrolling}
              setIsScrolling={setIsScrolling}
              showAuth={showAuth}
              setShowAuth={setShowAuth}
            />
          </Row>
          <Routes
            selected={selected}
            setSelected={setSelected}   
            isScrolling={isScrolling}
            setIsScrolling={setIsScrolling}
            startIndex={startIndex}
            setStartIndex={setStartIndex}
            endIndex={endIndex}
            setEndIndex={setEndIndex}
            end={end}
            setEnd={setEnd}
            withAccount={withAccount}
            setWithAccount={setWithAccount}
            showAuth={showAuth}
            setShowAuth={setShowAuth}
          />
          <Row className="footer-wrapper">
            <Footer
              setSelected={setSelected}
              withAccount={withAccount}
              setWithAccount={setWithAccount}
            />
          </Row>
        </Router>
      </Container>
    </UserProvider>
  );
}

export default App;
