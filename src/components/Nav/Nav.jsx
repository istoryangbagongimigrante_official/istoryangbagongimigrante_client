import React, { useEffect,useContext } from 'react'
import './Nav.css'
import { Link } from 'react-router-dom'
import UserContext from '../../utils/userContext.js'

export default function Nav({ selected,
								setSelected,
								showMobileNav,
								setShowMobileNav,
								isScrolling,
								setIsScrolling,
								setShowAuth
	}) {
	const {user,setUser,unsetUser} = useContext(UserContext)
	useEffect(() => {
		window.onscroll = function(){
			window.pageYOffset>0?setIsScrolling(true):setIsScrolling(false)
		}

		if(window.location.pathname==='/'){
			setSelected('home')
		} else if(window.location.pathname==="/stories/immigrant"||window.location.pathname==="/stories/teachers"){
			setSelected('stories')
		} else if(window.location.pathname==='/redirect'){
			return null
		} else {
			setSelected(window.location.pathname.substring(1))
		}
	}, [window.pageYOffset,showMobileNav,window.location.pathname,user])

	function logout(){
		setShowMobileNav(false)
		unsetUser()
 		setUser({
	 		email: null,
	 		isAdmin: null
	 	})
	}

	return (
		<div className={`nav ${isScrolling?"show":null} ${showMobileNav?"mobile-show":null}`}>
			<a href="/" className="brand">istoryangbagongimigrante</a>
			<div className="nav-menu">
				<ul>
					<li className="nav-item">
						<Link id="home" to="/"
							className={selected==="home"?'selected':null}
							onClick={(e)=>{
								setSelected(e.target.id)
								setShowMobileNav(false)
							}}
						>Home</Link>
					</li>
					<div className="spacer"></div>
					<li className="nav-item">
						<Link id="about" to="/about"
							className={selected==="about"?'selected':null}
							onClick={(e)=>{
								setSelected(e.target.id)
								setShowMobileNav(false)
							}}
						>About</Link>
					</li>
					<div className="spacer"></div>
					<li className="nav-item dropdown">
						<Link to="#" id="stories"
							className={selected==="stories"?'selected':null}
							onClick={(e)=>{setSelected(e.target.id)}}
						>Stories&nbsp;&nbsp;<i className="fas fa-caret-down"></i></Link>
						<div className="dropdown-content">
							<Link to="/stories/teachers"
								className="dropdown-content-link"
								onClick={(e)=>{
									setSelected('stories')
									setShowMobileNav(false)
								}}
							>
								Filipino Language Teachers
							</Link>
							<div className="dropdown-spacer"></div>
							<Link to="/stories/immigrant"
								className="dropdown-content-link"
								onClick={(e)=>{
									setSelected('stories')
									setShowMobileNav(false)
								}}
							>
								Immigrant Stories
							</Link>
						</div>
					</li>
					<div className="spacer"></div>
					<li className="nav-item">
						<Link id="travel" to="/travel"
							className={selected==="travel"?'selected':null}
							onClick={(e)=>{
								setSelected(e.target.id)
								setShowMobileNav(false)
							}}
						>Travel</Link>
					</li>
					<div className="spacer"></div>
					<li className="nav-item">
						<Link id="foods" to="/foods"
							className={selected==="foods"?'selected':null}
							onClick={(e)=>{
								setSelected(e.target.id)
								setShowMobileNav(false)
							}}
						>Foods</Link>
					</li>
					<div className="spacer"></div>
					<li className="nav-item">
						<Link id="news" to="/news"
							className={selected==="news"?'selected':null}
							onClick={(e)=>{
								setSelected(e.target.id)
								setShowMobileNav(false)
							}}
						>News</Link>
					</li>
					<div className="spacer"></div>
					<li className="nav-item">
						<Link id="tips" to="/tips"
							className={selected==="tips"?'selected':null}
							onClick={(e)=>{
								setSelected(e.target.id)
								setShowMobileNav(false)
							}}
						>Tips</Link>
					</li>
					<div className="spacer"></div>
					{
						user.isAdmin
						?
						<>
						<li className="nav-item">
							<Link id="dashboard" to="/dashboard"
								className={selected==="dashboard"?'selected':null}
								onClick={(e)=>{
									setSelected(e.target.id)
									setShowMobileNav(false)
								}}
							>Dashboard</Link>
						</li>
						<div className="spacer"></div>
						</>
						:
						null
					}
					{
						user.email===undefined||user.email===null
						?
						<li className="nav-item">
							<Link id="register" to="#"
								className={selected==="register"?'selected':null}
								onClick={(e)=>{
									setShowMobileNav(false)
									setShowAuth(true)
								}}
							>Register / Log in</Link>
						</li>
						:
						<li className="nav-item">
							<Link id="register" to="#"
								className={selected==="register"?'selected':null}
								onClick={logout}
							>Log out</Link>
						</li>
					}
				</ul>
			</div>
		</div>
	)
};