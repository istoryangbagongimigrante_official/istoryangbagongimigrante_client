import React from 'react'
import './Footer.css'
import { Link } from 'react-router-dom'

export default function Footer({ setSelected }) {
	return (
		<div className="footer">
			<div className="footer-top">
				<div className="footer-header">
					<h6>About Us</h6>
					<ul>
						<li>
							<Link to="/about" id="about" onClick={(e)=>setSelected(e.target.id)}>Who we are</Link>
						</li>
						<li className="socmed-links">
							Follow us on:
							<a href="https://www.facebook.com/Istoryangbagongimigrante/"
								target="_blank"
		            			rel="noopener noreferrer">
		            			<i className="fab fa-facebook-square"></i>
		            		</a>
							<a href="https://www.youtube.com/channel/UCQIkhosWtgZiOXO0Y2p0_ZQ"
								target="_blank"
		            			rel="noopener noreferrer"
							>
								<i className="fab fa-youtube-square"></i>
							</a>
						</li>
					</ul>
				</div>
				<div className="footer-header">
					<h6>Quick Links</h6>
					<ul>
						<li>
							<span><Link to="/" id="home" onClick={(e)=>setSelected(e.target.id)}>Home</Link></span>
							<span><Link to="/about" id="about" onClick={(e)=>setSelected(e.target.id)}>About</Link></span>
						</li>
						<li>
							<span><Link to="/stories/teachers" id="stories" onClick={(e)=>setSelected(e.target.id)}>Teachers</Link></span>
							<span><Link to="/stories/immigrant" id="stories" onClick={(e)=>setSelected(e.target.id)}>Immigrants</Link></span>
						</li>
						<li>
							<span><Link to="/travel" id="travel" onClick={(e)=>setSelected(e.target.id)}>Travel</Link></span>
							<span><Link to="/foods" id="foods" onClick={(e)=>setSelected(e.target.id)}>Foods</Link></span>
						</li>
						<li>
							<span><Link to="/news" id="news" onClick={(e)=>setSelected(e.target.id)}>News</Link></span>
							<span><Link to="/tips" id="tips" onClick={(e)=>setSelected(e.target.id)}>Tips</Link></span>
						</li>
					</ul>
				</div>
				<div className="footer-header">
					<h6>Contact Us</h6>
					<ul>
						<li>istoryangbagongimigrante@gmail.com</li>
					</ul>
				</div>
			</div>
			<div className="footer-bottom">&copy; <a href="/">istoryangbagongimigrante.com</a> 2021</div>
		</div>
	)
};