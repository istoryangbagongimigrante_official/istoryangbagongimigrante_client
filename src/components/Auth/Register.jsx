import React, { useState,useEffect } from 'react'
import './Auth.css'
import { Form,Button } from 'react-bootstrap'
import { toggleShowPassword,validateEmail,validatePassword } from '../../utils/utilFunctions.jsx'

export default function Register({ setWithAccount,setShowAuth }) {
	const [name,setName] = useState('')
	const [email,setEmail] = useState('')
	const [password,setPassword] = useState('')
	const [confirmPassword,setConfirmPassword] = useState('')
	const [showPassword,setShowPassword] = useState(true)
	const [focused,setFocused] = useState('')
	const [isClicked,setIsClicked] = useState(false)
	const [validName,setValidName] = useState(null)
	const [validEmail,setValidEmail] = useState(null)
	const [validPassword,setValidPassword] = useState(null)
	const [validConfirmPassword,setValidConfirmPassword] = useState(null)
	const [validRegister,setValidRegister] = useState(false)
	const [formMessage,setFormMessage] = useState('')
	const [formMessageTimeout,setFormMessageTimeout] = useState(null)

	function inputChecker(input){
		if(input==="name"){
			name.length>=5?setValidName(true):(name.length===0?setValidName(null):setValidName(false))
		}
		if(input==="email"){
			email.length!==0&&validateEmail.test(email.toString())?setValidEmail(true):(email.length===0?setValidEmail(null):setValidEmail(false))
		}
		if(input==="password"){
			password.match(validatePassword)?setValidPassword(true):(password.length===0?setValidPassword(null):setValidPassword(false));
			confirmPassword.length===0?setValidConfirmPassword(null):(validPassword&&confirmPassword===password?setValidConfirmPassword(true):setValidConfirmPassword(false));
		}
		if(input==="confirmPassword"){
			confirmPassword.length===0?setValidConfirmPassword(null):(validPassword&&confirmPassword===password?setValidConfirmPassword(true):setValidConfirmPassword(false))
		}
	}

	async function registerUser(e){
		e.preventDefault()
		setIsClicked(true)
		if(validName&&validEmail&&validPassword&&validConfirmPassword){
			setFormMessage('Registering')
			await fetch('https://istoryangbagongimigrante.herokuapp.com/api/users/register',{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json; charset=UTF-8',
				},
				body: JSON.stringify({
					username: name,
					email: email,
					password: password,
					confirmPassword: confirmPassword
				})
			})
			.then(res => res.json())
			.then(data=>{
				setFormMessage(data.message)
			})
			.catch(error => console.log(error))
		} else {
			setFormMessage('Please fill up the form.')
			setTimeout(function(){setFormMessage('')},3000)
		}
	}


	useEffect(()=>{
		let registerBtn = document.querySelector('.register-form-btn')
		if(focused&&!isClicked){
			inputChecker(focused)
		} else if(focused&&isClicked){
			setFocused('')
			setIsClicked(false)
		} else {
			return null
		}

		if(validName&&validEmail&&validPassword&&validConfirmPassword){
			registerBtn.disabled = false
			setValidRegister(true)
		} else {
			registerBtn.disabled = true
			setValidRegister(false)
		}
	},[focused,isClicked,name,email,password,confirmPassword,validName,validEmail,validPassword,validConfirmPassword,validRegister])

	useEffect(()=>{
		if(formMessage==='Registration Successful!'){
			setFormMessageTimeout(setTimeout(function(){
				setName('')
				setEmail('')
				setPassword('')
				setConfirmPassword('')
				setIsClicked(false)
				setValidName(null)
				setValidEmail(null)
				setValidPassword(null)
				setValidConfirmPassword(null)
				setValidRegister(false)
				setFormMessage('')
				setWithAccount(true)
			},2000))
		} else {
			clearTimeout(formMessageTimeout)
		}
	},[formMessage])

	return (
		<div className="register">
			<Form>
				<div className={`form-message ${formMessage==='Registration Successful!'?'success-msg':(formMessage==='Registering.'?'warning-msg':'fail-msg')}`}>{formMessage}</div>
				<i className="fas fa-times" onClick={()=>setShowAuth(false)}></i>
				<Form.Group className="mb-3 form-header">
			        <span>Register</span>
			        <span className="help-wrapper"><i className="far fa-question-circle"></i></span>
			        <div className="tool-tip-wrapper">
				        <div className="tool-tip">
				        	<p><span>Name:</span> must be atleast 5 characters</p>
				        	<p><span>Email:</span> must include @ and . <br/> e.g. (user@example.com)</p>
				        	<p><span>Password:</span> must be atleast 8 characters, 1 uppercase, 1 lowercase, alphanumeric and with special character/s
				        		<br/>
				        		(&emsp;!@#$%^&*)(_-+=]&#91;}&#123;;:'"&lt;>,.?/&emsp;)
				        	</p>
				        </div>
				    </div>
			    </Form.Group>
				<Form.Group className={`mb-3 form-div ${validName?'success':`${validName===false?'danger':null}`}`} controlid="name">
			        <Form.Control 
			        	type="text" 
			        	className="form-input" 
			        	placeholder=" "
			        	value={name}
			        	onChange={(e)=>setName(e.target.value)}
			        	onClick={(e)=>setFocused(e.target.id)}
			        	onKeyUp={(e)=>setFocused(e.target.id)}
			        	id="name"
			        />
			        <Form.Label className="form-label">Username</Form.Label>
			    </Form.Group>
			    <Form.Group className={`mb-3 form-div ${validEmail?'success':`${validEmail===false?'danger':null}`}`} controlid="email">
			        <Form.Control 
			        	type="email" 
			        	className="form-input" 
			        	placeholder=" "
			        	value={email}
			        	onChange={(e)=>setEmail(e.target.value)}
			        	onClick={(e)=>setFocused(e.target.id)}
			        	onKeyUp={(e)=>setFocused(e.target.id)}
			        	id="email"
			        />
			        <Form.Label className="form-label">Email</Form.Label>
			    </Form.Group>
  			    <Form.Group className={`mb-3 form-div ${validPassword?'success':`${validPassword===false?'danger':null}`}`} controlid="password">
  			        <Form.Control 
			        	type="password" 
			        	className="form-input" 
			        	placeholder=" "
			        	value={password}
			        	onChange={(e)=>setPassword(e.target.value)}
			        	onClick={(e)=>setFocused(e.target.id)}
			        	onKeyUp={(e)=>setFocused(e.target.id)}
			        	id="password"
			        />
  			        <Form.Label className="form-label">Password</Form.Label>
  			        {
  			        	showPassword
  			        	?
  			        	<i className="far fa-eye-slash" onClick={()=>{
  			        		toggleShowPassword('register')
  			        		setShowPassword(!showPassword)
  			        	}}></i>
  			        	:
  			        	<i className="far fa-eye" onClick={()=>{
  			        		toggleShowPassword('register')
  			        		setShowPassword(!showPassword)
  			        	}}></i>
  			        }  
  			    </Form.Group>
  			    <Form.Group className={`mb-3 form-div ${validConfirmPassword?'success':`${validConfirmPassword===false?'danger':null}`}`} controlid="confirmPassword">
  			        <Form.Control 
			        	type="password" 
			        	className="form-input" 
			        	placeholder=" "
			        	value={confirmPassword}
			        	onChange={(e)=>setConfirmPassword(e.target.value)}
			        	onClick={(e)=>setFocused(e.target.id)}
			        	onKeyUp={(e)=>setFocused(e.target.id)}
			        	id="confirmPassword"
			        />
  			        <Form.Label className="form-label">Confirm Password</Form.Label>
  			        {
  			        	showPassword
  			        	?
  			        	<i className="far fa-eye-slash" onClick={()=>{
  			        		toggleShowPassword('register')
  			        		setShowPassword(!showPassword)
  			        	}}></i>
  			        	:
  			        	<i className="far fa-eye" onClick={()=>{
  			        		toggleShowPassword('register')
  			        		setShowPassword(!showPassword)
  			        	}}></i>
  			        }
  			    </Form.Group>
  			    <Form.Group className="mb-3 form-switch">
  			        <span onClick={()=>setWithAccount(true)}>With account? Log in.</span>
  			    </Form.Group>
  			    <Button className="register-form-btn" onClick={registerUser}>
  			        Register
			    </Button>
			</Form>
		</div>
	)
};
