import React, { useState,useContext } from 'react'
import './Auth.css'
import { Form,Button } from 'react-bootstrap'
import { toggleShowPassword } from '../../utils/utilFunctions.jsx'
import UserContext from '../../utils/userContext.js'

export default function Login({ setWithAccount,setShowAuth,setIsAdmin }) {

	const {setUser} = useContext(UserContext)
	const [userLogin,setUserLogin] = useState('')
	const [userPassword,setUserPassword] = useState('')
	const [showUserPassword,setShowUserPassword] = useState(false)
	const [loginMessage,setLoginMessage] = useState('')

	async function loginUser(e){
		e.preventDefault()
		setLoginMessage('Verifying credentials.')
		if(userLogin&&userPassword){
			await fetch('https://istoryangbagongimigrante.herokuapp.com/api/users/login',{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					user: userLogin,
					password: userPassword
				})
			})
			.then(res => res.json())
			.then(data => {
				if(data.accessToken){
					setLoginMessage('Logging in.')
					localStorage.setItem('token', data.accessToken)
					fetch('https://istoryangbagongimigrante.herokuapp.com/api/users/getUser',{
						method: 'GET',
						headers: {
							Authorization: `Bearer ${data.accessToken}`
						}
					})
					.then(res => res.json())
					.then(data => {
						localStorage.setItem('user',data.username)
						localStorage.setItem('email',data.email)	
						setUser({
							email:data.email,
							isAdmin: data.isAdmin
						})
						setUserLogin('')
						setUserPassword('')
						setLoginMessage(data.message)
						setShowAuth(false)
					})
				} else {
					setLoginMessage(data.message)
				}
			})
		} else {
			setLoginMessage('Invalid credentials.')
		}
	}

	return (
		<div className="login">
			<Form>
				<div className={`form-message ${loginMessage==='Logging in.'||loginMessage==='Log in Successful!'?'success-msg':(loginMessage==='Verifying credentials.'?'warning-msg':'fail-msg')}`}>{loginMessage}</div>
				<i className="fas fa-times" onClick={()=>{setShowAuth(false)}}></i>
				<Form.Group className="mb-3 form-header">
					<span>Log in</span>
				</Form.Group>
				<Form.Group className="mb-3 form-div" controlid="email">
					<Form.Control
						type="email"
						className="form-input"
						placeholder=" "
						value={userLogin}
						onChange={(e)=>setUserLogin(e.target.value)}
					/>
					<Form.Label className="form-label">Email</Form.Label>
				</Form.Group>
				<Form.Group className="mb-3 form-div" controlid="password">
					<Form.Control
						type="password"
						className="form-input"
						placeholder=" "
						value={userPassword}
						onChange={(e)=>setUserPassword(e.target.value)}
						id="userPassword"
					/>
					<Form.Label className="form-label">Password</Form.Label>
					{
						showUserPassword
						?
						<i className="far fa-eye"
							onClick={()=>{
								toggleShowPassword('login')
								setShowUserPassword(!showUserPassword)
							}}
						></i>
						:
						<i className="far fa-eye-slash"
							onClick={()=>{
								toggleShowPassword('login')
								setShowUserPassword(!showUserPassword)
							}}
						></i>
					}
				</Form.Group>
				<Form.Group className="mb-3 form-switch">
					<span onClick={()=>setWithAccount(false)}>No account? Register.</span>
				</Form.Group>

				<Button className="login-form-btn"
					onClick={loginUser}
				>
					Log in
				</Button>		
			</Form>
		</div>
	)
};
