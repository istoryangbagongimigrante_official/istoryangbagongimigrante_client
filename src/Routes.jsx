import React from 'react'
import { Route, Switch } from 'react-router-dom'

import About from './pages/About/About.jsx'
import StoriesTeachers from './pages/Stories-Teachers/StoriesTeachers.jsx'
import News from './pages/News/News.jsx'
import Home from './pages/Home/Home.jsx'
import Travel from './pages/Travel/Travel.jsx'
import Foods from './pages/Foods/Foods.jsx'
import Tips from './pages/Tips/Tips.jsx'
import ImmigrantStories from './pages/ImmigrantStories/ImmigrantStories.jsx'
import Page404 from './pages/Page404/Page404.jsx'
import RedirectPage from './pages/Redirect/RedirectPage.jsx'
import Dashboard from './pages/Dashboard/Dashboard.jsx'
import Kaohsiung from './pages/Kaohsiung/Kaohsiung.jsx'
import Taichung from './pages/Taichung/Taichung.jsx'
import Taipei from './pages/Taipei/Taipei.jsx'
import Tainan from './pages/Tainan/Tainan.jsx'
import Hsinchu from './pages/Hsinchu/Hsinchu.jsx'
import Taoyuan from './pages/Taoyuan/Taoyuan.jsx'
import Keelung from './pages/Keelung/Keelung.jsx'
import Chiayi from './pages/Chiayi/Chiayi.jsx'
import Yilan from './pages/Yilan/Yilan.jsx'
import Miaoli from './pages/Miaoli/Miaoli.jsx'

export default function Routes({ selected, 
                                setSelected,
                                setIsScrolling,
                                isScrolling,
                                startIndex,
                                setStartIndex,
                                endIndex,
                                setEndIndex,
                                end,
                                setEnd,
                                withAccount,
                                setWithAccount,
                                showAuth,
                                setShowAuth,
    }) {
	return (
		<Switch>
			<Route exact path='/about'
                render={(props)=>(
                    <About {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                    />
                )}
            />
            <Route exact path='/stories/teachers'
                render={(props)=>(
                    <StoriesTeachers {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/foods'
                render={(props)=>(
                    <Foods {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/news'
                render={(props)=>(
                    <News {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/'
                render={(props)=>(
                    <Home {...props}
                        setSelected={setSelected}
                        setIsScrolling={setIsScrolling}
                        showAuth={showAuth}
                        setShowAuth={setShowAuth}
                    />
                )}
            />
            <Route exact path='/travel'
                render={(props)=>(
                    <Travel {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/tips'
                render={(props)=>(
                    <Tips {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/stories/immigrant'
                render={(props)=>(
                    <ImmigrantStories {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/redirect'
                render={(props)=>(
                    <RedirectPage {...props}
                        isScrolling={isScrolling}
                        selected={selected}
                        setIsScrolling={setIsScrolling}
                        setShowAuth={setShowAuth}
                    />
                )}
            />
            <Route exact path='/dashboard'
                render={(props)=>(
                    <Dashboard {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                    />
                )}
            />
            <Route exact path='/kaohsiung'
                render={(props)=>(
                    <Kaohsiung {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/taichung'
                render={(props)=>(
                    <Taichung {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/taipei'
                render={(props)=>(
                    <Taipei {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/tainan'
                render={(props)=>(
                    <Tainan {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/hsinchu'
                render={(props)=>(
                    <Hsinchu {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/taoyuan'
                render={(props)=>(
                    <Taoyuan {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/keelung'
                render={(props)=>(
                    <Keelung {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/chiayi'
                render={(props)=>(
                    <Chiayi {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/yilan'
                render={(props)=>(
                    <Yilan {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='/miaoli'
                render={(props)=>(
                    <Miaoli {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                        startIndex={startIndex}
                        setStartIndex={setStartIndex}
                        endIndex={endIndex}
                        setEndIndex={setEndIndex}
                        end={end}
                        setEnd={setEnd}
                    />
                )}
            />
            <Route exact path='*'
                render={(props)=>(
                  <Page404 {...props}
                        isScrolling={isScrolling}
                        setIsScrolling={setIsScrolling}
                  />
                )}
            />
		</Switch>
	)
}
;
