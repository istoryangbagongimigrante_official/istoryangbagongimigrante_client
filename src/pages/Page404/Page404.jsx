import React, { useEffect } from 'react'
import './Page404.css'
import { Link } from 'react-router-dom'

export default function Page404({ isScrolling,setIsScrolling }) {

	useEffect(() => {
		let footerComponent = document.querySelector('.footer')
		let page404Component = document.querySelector('.page404')
		page404Component.style.height = `calc(100vh - ${footerComponent.offsetHeight}px)`

		setIsScrolling(true)
		document.body.scrollTop = document.documentElement.scrollTop = 0;
	}, [isScrolling])

	return (
		<div className="page404">
			<div className="page404-content-wrapper">
				<img src="/assets/redirect_page/repairman.png" className="repairman" alt="www.vecteezy.com/free-vector/web-Web Vectors by Vecteezy"/>
				<h1>Error 404</h1>
				<p className="not-found">Oh no! The page you're trying to access is missing.</p>
				<p>While our repair guy is busy fixing the page, feel free to explore the other available pages.</p>
				<div className="link-selection">
					<div><Link to="/">home</Link></div>
					<div><Link to="/about">about</Link></div>
					<div><Link to="/stories/teachers">stories</Link></div>
					<div><Link to="/travel">travel</Link></div>
					<div><Link to="/foods">foods</Link></div>
					<div><Link to="/news">news</Link></div>
					<div><Link to="/tips">tips</Link></div>
				</div>
			</div>		
		</div>
	)
};
