import React,{ useEffect,useState,useContext } from 'react'
import './News.css'
import { newsData } from '../../utils/news.js'
import { Redirect } from 'react-router-dom'
import UserContext from '../../utils/userContext.js'

export default function Health({ setIsScrolling,
                                isScrolling,
                                startIndex,
                                setStartIndex,
                                endIndex,
                                setEndIndex,
                                end,
                                setEnd
    }) {

	const {user} = useContext(UserContext)

	const [paginatedNews,setPaginatedNews] = useState([])

	useEffect(() => {
		setIsScrolling(true)
		document.body.scrollTop = document.documentElement.scrollTop = 0;
		setStartIndex(1)
		setEndIndex(5)
		setEnd(false)
	}, [isScrolling])

	useEffect(()=>{
		setPaginatedNews(newsData.slice(startIndex-1,endIndex))

		if(endIndex>newsData.length||endIndex===newsData.length){
			setEnd(true)
			setEndIndex(newsData.length)
		} else {
			setEnd(false)
		}
	},[endIndex])

	return (
		user.email===undefined||user.email===null
		?
		<Redirect to="/redirect"/>
		:
		<div className="news">
			<div className="page-header">Latest News</div>
			{
				paginatedNews.map(entry=>(
					<div className="section" key={entry.id}>
						<div className="section-img">
							<img src={entry.img} alt={entry.title}/>
						</div>
						<div className="section-text">
							<div className="section-text-title">{entry.title}</div>
							<p className="section-text-desc">
								<span className="section-img-mobile">
									<img src={entry.img} alt={entry.title}/>
								</span>
								{entry.description}
							</p>
						</div>
					</div>
				))
			}
			<div className="show-more">
				{end?null:<span onClick={()=>setEndIndex(endIndex+5)}>show more</span>}
			</div>
		</div>
	)
};