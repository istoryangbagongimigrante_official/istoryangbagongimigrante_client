import React,{ useEffect,useContext } from 'react'
import './StoriesTeachers.css'
import { storiesTeachersData } from '../../utils/storiesTeachers.js'
import { Redirect,Link } from 'react-router-dom'
import UserContext from '../../utils/userContext.js'

export default function StoriesTeachers({ setIsScrolling,
                                isScrolling,
                                startIndex,
                                setStartIndex,
                                endIndex,
                                setEndIndex,
                                end,
                                setEnd,
    }) {
	const {user} = useContext(UserContext)

	useEffect(() => {
		setIsScrolling(true)
		document.body.scrollTop = document.documentElement.scrollTop = 0;
	}, [isScrolling])

	return (
		user.email===undefined||user.email===null
		?
		<Redirect to="/redirect"/>
		:
		<div className="storiesTeachers">
			<img src="/assets/teachers_stories_page/world_map.png" alt="world map with taiwan as focus" className="cities-bg"/>
			<div className="cities-content-wrapper">
				<div className="pop-ups">
					<div className="pop-ups-inner-wrapper">
						{
							storiesTeachersData.map(entry=>(
								<img src={entry.img} key={entry.id} alt={entry.id} className={`pop-up-${entry.id}`}/> 
							))
						}
					</div>
				</div>
				<h1>Teachers</h1>
				<p>Stories featuring Filipino language teachers from the major cities of Taiwan</p>
			</div>
			<div className="cities-links-wrapper">
				<div className="cities-padding-top"></div>
				{
					storiesTeachersData.map(entry=>(
					<Link to={`/${entry.city}`}
							key={entry.id}
							className="city-link-wrapper"
					>
						<span className="bordered cities-link" id={entry.city}>
							{entry.city}
						</span>
						<img id={entry.city} src={entry.img} alt={entry.name} 
						/>
					</Link>
					))
				}
			</div>
		</div>
		

	)
};