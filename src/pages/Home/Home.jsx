import React, {useEffect,useContext} from 'react'
import './Home.css'
import { Container,Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import $ from 'jquery'
import { contentsData } from '../../utils/home.js'
import UserContext from '../../utils/userContext.js'

export default function Home({ setSelected,setIsScrolling,showAuth,setShowAuth }) {

	const {user} = useContext(UserContext)

	useEffect(()=>{
		setIsScrolling(false)
		document.body.scrollTop = document.documentElement.scrollTop = 0;

		$(function() {
		  $("a.scroll").click(function() {
		    var anchor = $(this).attr("href");
		    $("html,body").stop().animate({
		      scrollTop: $(anchor).offset().top
		    }, 500);
		    return false;
		  });
		});

	},[])

	return (
		<Container fluid className="home">
			{
				user.email===undefined||user.email===null
				?
				<div className="register-btn" onClick={()=>setShowAuth(true)}>register / log in</div>
				:
				null
			}
			<Row className="landing">
				<div className="landing-images-wrapper">
					<div>
						<img src="/assets/home_page/teacherAu.jpg" alt="teacher 1" className="landing-img"/>
						<img src="/assets/home_page/teacherCindy.png" alt="teacher 3" className="landing-img"/>
					</div>
					<div>
						<img src="/assets/home_page/teacherLiberty.jpg" alt="teacher 2" className="landing-img"/>
						<img src="/assets/home_page/placeholder.jpg" alt="teacher 4" className="landing-img"/>
					</div>
				</div>

				<div className="landing-text-wrapper">
					<h1>istoryang bagong imigrante</h1>
					<div className="landing-quick-links-wrapper">
						<Link to="/foods" className="landing-quick-links" id="foods" onClick={(e)=>setSelected(e.target.id)}>Foods</Link>
						<i className="fas fa-circle"></i>
						<Link to="/stories/teachers" className="landing-quick-links" id="stories" onClick={(e)=>setSelected(e.target.id)}>stories</Link>
						<i className="fas fa-circle"></i>
						<Link to="/travel" className="landing-quick-links" id="travel" onClick={(e)=>setSelected(e.target.id)}>Travel</Link>
					</div>
					<p>Travel guide, stress relief, motivation and a source of information for Filipino language teachers and new migrants in Taiwan.</p>
					<div className="landing-arrow-down">
						<a href="#contents" className="scroll">
							<i className="fas fa-chevron-down"></i>
						</a>
					</div>
				</div>
			</Row>
			<Row className="contents" id="contents">
				{
					contentsData.map(entry=>(
						<div className="content-card" key={entry.id}>
							<img src={entry.img} alt={entry.tag} className="content-img"/>
							<div className="content-text">{entry.description}</div>
							<Link to={entry.path} className="content-tag">{entry.tag}</Link>
						</div>
					))
				}
			</Row>
		</Container>
	)
};