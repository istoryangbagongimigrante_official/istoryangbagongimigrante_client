import React, { useEffect,useState,useContext } from 'react'
import './RedirectPage.css'
import { Redirect } from 'react-router-dom'
import UserContext from '../../utils/userContext.js'

export default function RedirectPage({ setShowAuth,selected,isScrolling,setIsScrolling }) {

	const {user} = useContext(UserContext)

	const [redirectPath,setRedirectPath] = useState('')

	useEffect(()=>{
		if(window.location.pathname==='/redirect'){
			let footerComponent = document.querySelector('.footer')
			let redirectComponent = document.querySelector('.redirectPage')
			redirectComponent.style.height = `calc(100vh - ${footerComponent.offsetHeight}px)`
		}

		if(selected==='home'){
			setRedirectPath('/')
		} else if(selected==='stories'){
			setRedirectPath('/stories/teachers')
		} else {
			setRedirectPath(selected)
		}

		setIsScrolling(true)
		document.body.scrollTop = document.documentElement.scrollTop = 0;

	},[window.location.pathname,isScrolling,selected])

	return (
		user.email===undefined||user.email===null
		?
		<div className="redirectPage">
			<div className="redirect-images">
				<img src="/assets/redirect_page/joinforce.jpg" alt="By Dio Hasbi Saniskoro from Pexels"/>
				<img src="/assets/redirect_page/joinus.jpg" alt="By Linda Eller-Shein from Pexels"/>
			</div>
			<div className="redirect-text">
				<div className="redirect-message">You are not logged in.<br/> Please login first before you can view the contents of this website.<br/>Don't worry it's free.</div>
				<span className="redirect-register-btn" onClick={()=>setShowAuth(true)}>register / login</span>
			</div>
		</div>
		:
		<Redirect to={redirectPath}/>
	)
};
