import React,{ useEffect,useState,useContext } from 'react'
import './Dashboard.css'
import { Table, Container } from 'react-bootstrap'
import { Redirect } from 'react-router-dom'
import UserContext from '../../utils/userContext.js'

export default function Dashboard({ isScrolling,setIsScrolling }) {

	const {user} = useContext(UserContext)
	const [userCount,setUserCount] = useState(0)
	const [users,setUsers] = useState([])
	const [toggleFetch,setToggleFetch] = useState(false)
	const [showDashMsg,setShowDashMsg] = useState(false)
	const [showDashMsgNoUser,setShowDashMsgNoUser] = useState(false)

	useEffect(() => {
		setIsScrolling(true)
		document.body.scrollTop = document.documentElement.scrollTop = 0;
	}, [isScrolling])

	useEffect(()=>{
		fetch('https://istoryangbagongimigrante.herokuapp.com/api/users/getAll',{
			method: 'GET',
			headers: {	
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUserCount(parseInt(data.usersCount))
			if(data.users){
				setUsers(data.users.reverse())
			} else {
				return null
			}
		})
		.catch(error => console.log(error))
	},[toggleFetch,userCount])

	async function deleteUser(userId){
			if(userCount===0){
				setShowDashMsg(true)
				setTimeout(()=>{setShowDashMsg(false)},1300)
			} else {
				await fetch(`https://istoryangbagongimigrante.herokuapp.com/api/users/${userId}`,{
					method: 'DELETE',
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}	
				})
				.then(res => res.json())
				.then(data => {
					setToggleFetch(!toggleFetch)
					setShowDashMsg(true)
					setTimeout(()=>{setShowDashMsg(false)},1300)
				})
				.catch(error => console.log(error))
			}
	}

	function noUser(userId){
			setShowDashMsgNoUser(true)
			setTimeout(()=>{setShowDashMsgNoUser(false)},1300)
	}

	return (
		user.isAdmin
		?
		<>
			<div className={`dashboard-message ${showDashMsg?'show-dash-message':null}`}>User has been deleted!</div>
			<div className={`dashboard-message-no-user ${showDashMsgNoUser?'show-dash-message':null}`}>No user to delete!</div>
			<Container className="dashboard">
				<Table striped bordered hover>
				    <thead>
				    	<tr className="dashboard-header">
							<th colSpan="2">Admin Dashboard</th>
							<th className="user-count-wrapper"># of users: <span className="user-count">{userCount}</span></th>
				    	</tr>
				    </thead>
				    <thead>
				    	<tr>
							<th>username</th>
							<th>email</th>
							<th>action</th>
				    	</tr>
				    </thead>
				    <tbody>
				    	{
				    		userCount===0
				    		?
				    		<tr key={user._id}>
			    				<td className="user-name">no user</td>
			    				<td className="user-email">-------------</td>
			    				<td className="user-delete-btn-wrapper">
			    					<i id={user._id} className="far fa-trash-alt" onClick={()=>noUser()}></i>
			    				</td>
			    			</tr>	
				    		:
				    		<>
				    		{
					    		users.map(user => (
					    			<tr key={user._id}>
					    				<td className="user-name">{user.username}</td>
					    				<td className="user-email">{user.email}</td>
					    				<td className="user-delete-btn-wrapper">
					    					<i id={user._id} 
							        			className="far fa-trash-alt"
							        			onClick={(e)=>deleteUser(e.target.id)}
							        		></i>
					    				</td>
					    			</tr>
					    		))
					    	}
					    	</>
				    	}
				    </tbody>
				</Table>
			</Container>
		</>
		:
		<Redirect to="/redirect"/>
	)

};


