import React,{ useEffect,useContext } from 'react'
import './Taipei.css'
import { Redirect } from 'react-router-dom'
import { taipeiStoriesData } from '../../utils/taipei.js'
import UserContext from '../../utils/userContext.js'

export default function Taipei({ 
										isScrolling,
										setIsScrolling
	}) {

	const {user} = useContext(UserContext)

	useEffect(() => {
		setIsScrolling(true)
		document.body.scrollTop = document.documentElement.scrollTop = 0;
	}, [isScrolling])

	return (
		user.email===undefined||user.email===null
		?
		<Redirect to="/redirect"/>
		:
		<div className="taipei">
			<div className="page-header">Taipei City</div>
			<div className="city-story-wrapper">
				{
					taipeiStoriesData.map(story=>(
						<div className="city-story-div" key={story.id}>						
							<img src={story.img} alt={story.alt} className="story-img"/>
							<div className="story-text">
								"{story.text}"
								<div className="story-location">- {story.location} -</div>
							</div>

						</div>
					))
				}	
			</div>
		</div>
	)
};
