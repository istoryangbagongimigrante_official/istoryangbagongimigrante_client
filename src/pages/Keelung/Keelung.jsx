import React,{ useEffect,useContext } from 'react'
import './Keelung.css'
import { Redirect } from 'react-router-dom'
import { keelungStoriesData } from '../../utils/keelung.js'
import UserContext from '../../utils/userContext.js'

export default function Keelung({ 
										isScrolling,
										setIsScrolling,
	}) {
	const {user} = useContext(UserContext)

	useEffect(() => {
		setIsScrolling(true)
		document.body.scrollTop = document.documentElement.scrollTop = 0;
	}, [isScrolling])
 
	return (
		user.email===undefined||user.email===null
		?
		<Redirect to="/redirect"/>
		:
		<div className="keelung">
			<div className="page-header">Keelung City</div>
			<div className="city-story-wrapper">
				{
					keelungStoriesData.map(story=>(
						<div className="city-story-div" key={story.id}>						
							<img src={story.img} alt={story.alt} className="story-img"/>
							<div className="story-text">
								"{story.text}"
								<div className="story-location">- {story.location} -</div>
							</div>
						</div>
					))
				}	
			</div>
		</div>
	)
};
