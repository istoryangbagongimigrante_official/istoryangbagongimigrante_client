import React,{ useEffect,useState,useContext } from 'react'
import './Foods.css'
import { foodsData } from '../../utils/foods.js'
import { Redirect } from 'react-router-dom'
import UserContext from '../../utils/userContext.js'

export default function Foods({ setIsScrolling,
                                isScrolling,
                                startIndex,
                                setStartIndex,
                                endIndex,
                                setEndIndex,
                                end,
                                setEnd
    }) {
	const {user} = useContext(UserContext)

	const [paginatedFoods,setPaginatedFoods] = useState([])

	useEffect(() => {
		setIsScrolling(true)
		document.body.scrollTop = document.documentElement.scrollTop = 0;
		setStartIndex(1)
		setEndIndex(5)
		setEnd(false)
	}, [isScrolling])

	useEffect(()=>{
		setPaginatedFoods(foodsData.slice(startIndex-1,endIndex))

		if(endIndex>foodsData.length||endIndex===foodsData.length){
			setEnd(true)
			setEndIndex(foodsData.length)
		} else {
			setEnd(false)
		}
	},[endIndex])

	return (
		user.email===undefined||user.email===null
		?
		<Redirect to="/redirect"/>
		:
		<div className="foods">
			<div className="page-header">Must Try Foods in Taiwan</div>
			{
				paginatedFoods.map(entry=>(
					<div className="section" key={entry.id}>
						<div className="section-img">
							<img src={entry.img} alt={entry.alt}/>
						</div>
						<div className="section-text">
							<div className="section-text-title">{entry.title}</div>
							<p className="section-text-desc">
								<span className="section-img-mobile">
									<img src={entry.img} alt={entry.alt}/>
								</span>
								{entry.description}
							</p>
							<div className="search-item">
								<a href={entry.search} target="_blank" rel="noopener noreferrer">
									<span className="know-more">Search: <span>{entry.title.toLowerCase()}</span> <i className="fas fa-search"></i></span>
								</a>
							</div>
						</div>
					</div>
				))
			}
			<div className="show-more">
				{end?null:<span onClick={()=>setEndIndex(endIndex+5)}>show more</span>}
			</div>
		</div>	
	)
};
