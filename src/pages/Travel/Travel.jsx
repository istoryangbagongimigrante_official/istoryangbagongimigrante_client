import React,{ useEffect,useContext } from 'react'
import './Travel.css'
import { travelData } from '../../utils/travel.js'
import { Redirect } from 'react-router-dom'
import UserContext from '../../utils/userContext.js'

export default function Travel({ setIsScrolling,
                                isScrolling,
    }) {

	const {user} = useContext(UserContext)

	useEffect(() => {
		setIsScrolling(true)
		document.body.scrollTop = document.documentElement.scrollTop = 0;
	}, [isScrolling])

	return (
		user.email===undefined||user.email===null
		?
		<Redirect to="/redirect"/>
		:
		<div className="travel">
			<div className="page-header">Famous spots in Taiwan</div>
			<div className="city-story-wrapper">
				{
					travelData.map(story=>(
						<div className="city-story-div" key={story.id}>						
							<img src={story.img} alt={story.alt} className="story-img"/>
							<div className="story-text">
								"{story.text}"
								<div className="story-location">- {story.location} -</div>
							</div>

						</div>
					))
				}	
			</div>
		</div>	
	)
};
