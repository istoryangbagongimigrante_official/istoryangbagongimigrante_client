import React,{ useEffect } from 'react'
import './About.css'

export default function About({ isScrolling,setIsScrolling }) {

	useEffect(() => {
		if(window.location.pathname==='/about'){
			let footerComponent = document.querySelector('.footer')
			let aboutComponent = document.querySelector('.about')
			aboutComponent.style.height = `calc(100vh - ${footerComponent.offsetHeight}px - 2px)`
		}
		setIsScrolling(true)

	}, [window.location.pathname,isScrolling])

	return (
		<div className="about">
				<div className="about-img-container">
					<img src="/assets/about_page/about-us.jpg" alt="Filipino teachers in Taiwan" className="about-img"/>
				</div>
				<div className="about-text-container">
					<div className="about-header"><span>Who we are.</span></div>
					<div className="about-paragraph">
						We are a group of Filipinos, whose mission is to care, inspire, motivate and support new fellow Filipino Immigrants in Taiwan.
					</div>
					<div className="about-header"><span>What we aim for.</span></div>
					<div className="about-paragraph">
						<p>A motivational and inspirational website for Filipino language teachers in Taiwan as their means of stress prevention, intervention and their source of information.
						</p>
						<p>
							Designed specifically for new migrants in Taiwan intended to build connection and motivation among Filipino migrants abroad trying out for greener pastures in a foreign place.
						</p>
					</div>
				</div>
		</div>
	)
};