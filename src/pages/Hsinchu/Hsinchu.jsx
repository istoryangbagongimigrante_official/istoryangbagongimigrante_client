import React,{ useEffect,useContext } from 'react'
import './Hsinchu.css'
import { Redirect } from 'react-router-dom'
import { hsinchuStoriesData } from '../../utils/hsinchu.js'
import UserContext from '../../utils/userContext.js'

export default function Hsinchu({ 
										isScrolling,
										setIsScrolling,
	}) {

	const {user} = useContext(UserContext)

	useEffect(() => {
		setIsScrolling(true)
		document.body.scrollTop = document.documentElement.scrollTop = 0;
	}, [isScrolling])

	return (
		user.email===undefined||user.email===null
		?
		<Redirect to="/redirect"/>
		:
		<div className="hsinchu">
			<div className="page-header">Hsinchu City</div>
			<div className="city-story-wrapper">
				{
					hsinchuStoriesData.map(story=>(
						<div className="city-story-div" key={story.id}>						
							<img src={story.img} alt={story.alt} className="story-img"/>
							<div className="story-text">
								"{story.text}"
								<div className="story-location">- {story.location} -</div>
							</div>

						</div>
					))
				}	
			</div>
		</div>	
	)
};
