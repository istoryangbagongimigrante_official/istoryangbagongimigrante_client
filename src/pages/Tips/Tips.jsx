import React,{ useEffect,useState,useContext } from 'react'
import './Tips.css'
import { tipsData } from '../../utils/tips.js'
import { Redirect } from 'react-router-dom'
import UserContext from '../../utils/userContext.js'

export default function Tips({ setIsScrolling,
                                isScrolling,
                                startIndex,
                                setStartIndex,
                                endIndex,
                                setEndIndex,
                                end,
                                setEnd
    }) {

	const {user} = useContext(UserContext)

	const [paginatedTips,setPaginatedTips] = useState([])

	useEffect(() => {
		setIsScrolling(true)
		document.body.scrollTop = document.documentElement.scrollTop = 0;
		setStartIndex(1)
		setEndIndex(5)
		setEnd(false)
	}, [isScrolling])

	useEffect(()=>{
		setPaginatedTips(tipsData.slice(startIndex-1,endIndex))

		if(endIndex>tipsData.length||endIndex===tipsData.length){
			setEnd(true)
			setEndIndex(tipsData.length)
		} else {
			setEnd(false)
		}
	},[endIndex])

	return (
		user.email===undefined||user.email===null
		?
		<Redirect to="/redirect"/>
		:
		<div className="tips">
			<div className="page-header">Tips for New Immigrants</div>
			{
				paginatedTips.map(entry=>(
					<div className="section" key={entry.id}>
						<div className="section-img">
							<img src={entry.img} alt={entry.alt}/>
						</div>
						<div className="section-text">
							<div className="section-text-title">{entry.title}</div>
							<p className="section-text-desc">
								<span className="section-img-mobile">
									<img src={entry.img} alt={entry.alt}/>
								</span>
								{entry.description}
							</p>
						</div>
					</div>
				))
			}
			<div className="show-more">
				{end?null:<span onClick={()=>setEndIndex(endIndex+5)}>show more</span>}
			</div>
		</div>
	)
};