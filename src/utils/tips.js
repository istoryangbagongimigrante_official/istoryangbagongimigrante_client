let id = 0

export const tipsData = [
	{
		id: id=id+=1,
		title: "Tip #1",
		description: "The combination of slowly braised beef with a tangle of slurpable noodles seems so fundamental, it’s hard to believe any one culture can lay claim to it. It is, however, considered the national dish of Taiwan (though its origins are Chinese), and given a distinctly Taiwanese spin with the addition of pickled mustard greens and the signature five-spice powder of star anise, cloves, Chinese cinnamon, Sichuan pepper, and fennel seeds.",
		img: "/assets/tips_page/placeholder.jpg",
		alt:"placeholder"
	},
	{
		id: id=id+=1,
		title: "Tip #2",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/tips_page/placeholder.jpg",
		alt:"placeholder"
	},
	{
		id: id=id+=1,
		title: "Tip #3",
		description: "The combination of slowly braised beef with a tangle of slurpable noodles seems so fundamental, it’s hard to believe any one culture can lay claim to it. It is, however, considered the national dish of Taiwan (though its origins are Chinese), and given a distinctly Taiwanese spin with the addition of pickled mustard greens and the signature five-spice powder of star anise, cloves, Chinese cinnamon, Sichuan pepper, and fennel seeds.",
		img: "/assets/tips_page/placeholder.jpg",
		alt:"placeholder"
	},
	{
		id: id=id+=1,
		title: "Tip #4",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/tips_page/placeholder.jpg",
		alt:"placeholder"
	},
	
]