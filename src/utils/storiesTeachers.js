let id = 0

export const storiesTeachersData = [
	{
		id: id=id+=1,
		city: "kaohsiung",
		img: "/assets/teachers_stories_page/placeholder.jpg",
		alt: "teacher au"
	},
	{
		id: id=id+=1,
		city: "taichung",
		img: "/assets/teachers_stories_page/placeholder.jpg",
		alt: "teacher liberty"
	},
	{
		id: id=id+=1,
		city: "taipei",
		img: "/assets/teachers_stories_page/taipei.jpg",
		alt: "teacher cindy"
	},
	{
		id: id=id+=1,
		city: "tainan",
		img: "/assets/teachers_stories_page/placeholder.jpg",
		alt: "teacher's name 4"
	},
	{
		id: id=id+=1,
		city: "hsinchu",
		img: "/assets/teachers_stories_page/hsinchu.jpg",
		alt: "teacher's name 5"
	},
	{
		id: id=id+=1,
		city: "taoyuan",
		img: "/assets/teachers_stories_page/placeholder.jpg",
		alt: "teacher's name 6"
	},
	{
		id: id=id+=1,
		city: "keelung",
		img: "/assets/teachers_stories_page/placeholder.jpg",
		alt: "teacher's name 7"
	},
	{
		id: id=id+=1,
		city: "chiayi",
		img: "/assets/teachers_stories_page/placeholder.jpg",
		alt: "teacher's name 8"
	},
	{
		id: id=id+=1,
		city: "yilan",
		img: "/assets/teachers_stories_page/placeholder.jpg",
		alt: "teacher's name 9"
	},
	{
		id: id=id+=1,
		city: "miaoli",
		img: "/assets/teachers_stories_page/placeholder.jpg",
		alt: "teacher's name 10"
	},
	
]