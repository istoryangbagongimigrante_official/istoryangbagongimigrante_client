let id = 0

export const foodsData = [
	{
		id: id=id+=1,
		title: "Taiwanese Fried Chicken",
		description: "Taiwanese fried chicken is twice cooked for a crumbly and crunchy exterior but delicate as a tempura. Typically tossed with five-spice powder, salt, pepper and basil leaves for a flavorful and aromatic finish. Can be found two ways, popcorn-style and large cutlets. Definitely a good start to explore taiwanese cuisine since we are all familiar with fried chicken.",
		img: "/assets/foods_page/placeholder.jpg",
		search: "https://www.google.com/search?q=taipei+101+building",
		alt: "placeholder"
	},
	{
		id: id=id+=1,
		title: "Xiao Long Bao",
		description: "Commonly known as soup dumplings. Smooth and fluffy bun filled with juicy meat mixed with cubed soup jelly which when heated will melt into a soup. Unlike other dumplings, which are usually eaten in one go, soup dumplings are eaten by first, tearing or biting a small portion of the wrapper, slurp the soup, add a dip of your choice, then finally, eat the while dumpling.",
		img: "/assets/travel_page/placeholder.jpg",
		search: "https://www.google.com/search?q=national+palace+museum+taiwan",
		alt: "placeholder"
	},
	{
		id: id=id+=1,
		title: "Scallion Pancakes",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/travel_page/placeholder.jpg",
		search: "https://www.google.com/search?q=taroko+gorge",
		alt: "placeholder"
	},
	{
		id: id=id+=1,
		title: "Braised pork with rice",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/travel_page/placeholder.jpg",
		search: "https://www.google.com/search?q=yushan+national+park",
		alt: "placeholder"
	},
	{
		id: id=id+=1,
		title: "Gua bao",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/travel_page/placeholderplaceholder.jpg",
		search: "https://www.google.com/search?q=alishan+scenic+mountain+area",
		alt: "placeholder"
	},
	{
		id: id=id+=1,
		title: "Oyster omelet",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/travel_page/placeholderplaceholder.jpg",
		search: "https://www.google.com/search?q=sun+moon+lake",
		alt: "placeholder"
	},
	{
		id: id=id+=1,
		title: "Stinky Tofu",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/travel_page/placeholderplaceholder.jpg",
		search: "https://www.google.com/search?q=maokong",
		alt: "placeholder"
	},
	{
		id: id=id+=1,
		title: "Zong Zi",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/travel_page/placeholder.jpg",
		search: "https://www.google.com/search?q=dragon+tiger+pagodas",
		alt: "placeholder"
	},
	{
		id: id=id+=1,
		title: "Hot pot",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/travel_page/placeholder.jpg",
		search: "https://www.google.com/search?q=maolin+national+scenic+area",
		alt: "placeholder"
	},
	{
		id: id=id+=1,
		title: "Pineapple Cake",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/travel_page/placeholder.jpg",
		search: "https://www.google.com/search?q=rainbow+villae+taiwan",
		alt: "placeholder"
	},
	{
		id: id=id+=1,
		title: "Pearl Milk Tea",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/travel_page/placeholder.jpg",
		search: "https://www.google.com/search?q=fo+guang+shan+buddha+museum",
		alt: "placeholder"
	},
	{
		id: id=id+=1,
		title: "Intestine and oyster vermicelli",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/travel_page/placeholder.jpg",
		search: "https://www.google.com/search?q=chang+kai+shek+memorial+hall",
		alt: "placeholder"
	},
	{
		id: id=id+=1,
		title: "San Bei Ji",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/travel_page/placeholder.jpg",
		search: "https://www.google.com/search?q=longshan+temple+taiwan",
		alt: "placeholder"
	},
	{
		id: id=id+=1,
		title: "Fan tuan",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/travel_page/placeholder.jpg",
		search: "https://www.google.com/search?q=yangmingshan+geysers+taiwan",
		alt: "placeholder"
	},
	{
		id: id=id+=1,
		title: "Aiyu Jelly",
		description: "Every season is hot pot season in Taiwan. So central is hot pot to the dining culture here that most home kitchens are equipped with their own dedicated hot pot burners that get pulled out for company. There are nearly 5,000 hot pot restaurants countrywide, doling out one of a variety of styles — from shabu-shabu to Sichuan numbing mala to Taiwanese stinky tofu — with vibes that range from fast-food efficiency to all-you-can-eat fancy.",
		img: "/assets/travel_page/placeholder.jpg",
		search: "https://www.google.com/search?q=shilin+night+market",
		alt: "placeholder"
	},


]