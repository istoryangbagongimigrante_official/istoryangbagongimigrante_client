let id = 0

export const contentsData = [
	{
		id: id=id+=1,
		tag: "teachers",
		description: "Cupidatat est elit ut consectetur eiusmod excepteur nostrud aliqua fugiat do Proident tempor voluptate exercitation ut enim pariatur est mollit dolor ut commodo eiusmod proident nostrud ex ex id..",
		img: "/assets/home_page/stories.jpg",
		path: "/stories/teachers",
		alt: "Filipino teachers in Taiwan"
	},
	{
		id: id=id+=1,
		tag: "immigrants",
		description: "Dolor commodo anim anim minim do sunt sunt sit eiusmod mollit Eiusmod id ut nulla quis ullamco dolore culpa anim mollit laborum occaecat sit.",
		img: "/assets/home_page/immigrants.jpg",
		path: "/stories/immigrant",
		alt: "Photo by Spencer Davis from Pexels"
	},
	{
		id: id=id+=1,
		tag: "travel",
		description: "Dolor commodo anim anim minim do sunt sunt sit eiusmod mollit Eiusmod id ut nulla quis ullamco dolore culpa anim mollit laborum occaecat sit.",
		img: "/assets/home_page/travel.jpg",
		path: "/travel",
		alt: "Photo by Timo Volz from Pexels"
	},
	{
		id: id=id+=1,
		tag: "foods",
		description: "Dolor commodo anim anim minim do sunt sunt sit eiusmod mollit Eiusmod id ut nulla quis ullamco dolore culpa anim mollit laborum occaecat sit.",
		img: "/assets/home_page/foods.jpg",
		path: "/foods",
		alt: "Photo by Madison Inouye from Pexels"
	},
	{
		id: id=id+=1,
		tag: "news",
		description: "Dolor commodo anim anim minim do sunt sunt sit eiusmod mollit Eiusmod id ut nulla quis ullamco dolore culpa anim mollit laborum occaecat sit.",
		img: "/assets/home_page/news.jpg",
		path: "/news",
		alt: "Photo by Ekrulila from Pexels"
	},
	{
		id: id=id+=1,
		tag: "tips",
		description: "Dolor commodo anim anim minim do sunt sunt sit eiusmod mollit Eiusmod id ut nulla quis ullamco dolore culpa anim mollit laborum occaecat sit.",
		img: "/assets/home_page/tips.png",
		path: "/tips",
		alt: "Image by Taiwan Tourism Bureau"
	},
	
]