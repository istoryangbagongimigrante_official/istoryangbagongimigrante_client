export const capitalizeName = (name) => {
	name = name.toLowerCase().split(' ')
	for(let i=0;i<name.length;i++){
		name[i] = name[i].charAt(0).toUpperCase() + (name[i].slice(1));
	}
	return name.join(' ')
}

export const validateEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const validatePassword = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[ !@#$%^&*)(_\-+=}{\][\\|:;"'><,.?/]).{4,}$/

export const toggleShowPassword = (form) => {
	let passField = document.querySelector('#password')
	let confirmField = document.querySelector('#confirmPassword')
	let userPassField = document.querySelector('#userPassword')
	
	if(form==='login'){
		userPassField.type==='password'?userPassField.type='text':userPassField.type='password'
	} else if(form==='register'){
		passField.type==='password'?passField.type='text':passField.type='password'
		confirmField.type==='password'?confirmField.type='text':confirmField.type='password'
	}
	
}



export const selectAll = () => {
	let checkBoxes = document.getElementsByName('dashboard-checkbox')
	for(let i=0;i<checkBoxes.length;i++){
		if(checkBoxes[i].type==='checkbox'){
			checkBoxes[i].checked=true
		}
	}
}

export const deselectAll = () => {
	let checkBoxes = document.getElementsByName('dashboard-checkbox')
	for(let i=0;i<checkBoxes.length;i++){
		if(checkBoxes[i].type==='checkbox'){
			checkBoxes[i].checked=false
		}
	}
}